provider "aws" {
  region = "us-east-1"
}

variable "instance_types" {
  default = ["t2.micro", "t2.small", "t2.medium"]
}

variable "ami_ids" {
  default = ["ami-0sdfg5b159cbfafe1f0", "ami-hfgdgjdf7890abcdef0", "ami-abcdefdfgdhy97840"]
}

resource "aws_instance" "example" {
  count = length(var.instance_types)

  ami           = var.ami_ids[count.index]
  instance_type = var.instance_types[count.index]

  tags = {
    Name = "example-instance-${count.index}"
  }
}

