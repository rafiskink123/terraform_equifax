provider "aws" {
  region = "us-east-1"
}

variable "instance_types" {
  default = ["t2.micro", "t2.small", "t2.large"]
}

variable "ami_ids" {
  default = ["ami-0c55b159cbfafe1f0", "ami-sfgfdy89dd890abcdef0", "ami-jgdvjfhgfv890239n"]
}

locals {
  instance_name = "example-instance"
}

resource "aws_instance" "example" {
  count = length(var.instance_types)

  ami           = var.ami_ids[count.index]
  instance_type = var.instance_types[count.index]

  tags = {
    Name = "${local.instance_name}-${count.index}"
    Label = "Label-${count.index}"
  }
}

